# Ansible role - Deploy Worker
This role provides installation of Cryton Worker using Docker, pip, or Poetry.  
First, it checks if the appropriate user is created, installs Docker or pip and then installs the app.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```
- Ansible variables (do not disable `gather_facts` directive)

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

It is recommended to update the `cryton_worker_environment` variable.

For example:
```yaml
      cryton_worker_environment:
        MY_VARIABLE: my_value
```

| variable                           | description                                                                                                                                                          |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| cryton_worker_git_repository       | Origin repository.                                                                                                                                                   |
| cryton_worker_repository_branch    | Which version to use.                                                                                                                                                |
| cryton_worker_version              | Which version to use.                                                                                                                                                |
| run_as_user                        | Which user to use/create.                                                                                                                                            |
| cryton_worker_app_directory        | App directory path.                                                                                                                                                  |
| cryton_worker_executable_directory | Path to the directory which contains the executable.                                                                                                                 |
| cryton_worker_installation         | Type of the installation. Possible options are `pip`, `docker`, and `development`.                                                                                   |
| cryton_worker_environment          | Dictionary with the settings. More information can be found [here](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/components/worker/#settings). |
| cryton_worker_output_file          | Path to the file in which we will store the program output.                                                                                                          |
| cryton_worker_command_options      | Options to use when starting the app using pip.                                                                                                                      |
| cryton_worker_docker_compose_file  | Name of the Compose file with the app and its prerequisites.                                                                                                         |
| cryton_modules_git_repository      | Modules' origin repository.                                                                                                                                          |
| cryton_modules_directory           | Modules' directory path.                                                                                                                                             |
| cryton_modules_repository_branch   | Which modules' version to use.                                                                                                                                       |

## Examples

### Usage
**Pip installation:**
```yaml
  roles:
    - role: deploy-worker
      become: yes

```

**Docker installation:**
```yaml
  roles:
    - role: deploy-worker
      become: yes
      run_as_user: root
      cryton_worker_installation: docker

```

**Poetry installation:**
```yaml
  roles:
    - role: deploy-worker
      become: yes
      cryton_worker_installation: development

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: deploy-worker
  src: https://gitlab.ics.muni.cz/cryton/ansible/deploy-worker.git
  version: "master"
  scm: git
```
